package pages.secondaryPages;

import common.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.BasePage;

import java.util.logging.Logger;

public class IframePage extends BasePage {
    private static final PageElement editorLocator = new PageElement("Editor Locator", By.id("tinymce"), true);
    private static final PageElement frame  = new PageElement("iFrame", By.tagName("iframe"), true);

    public IframePage(WebDriver driver, Logger log) {
        super(driver, log);
    }

    public String getEditorText(){
        switshToFrame(frame);
        String text = find(editorLocator).getText();
        log.info("Text from TinyMCE Editor: " + text);
        return text;
    }

}
