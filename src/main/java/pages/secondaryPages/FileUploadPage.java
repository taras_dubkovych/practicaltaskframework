package pages.secondaryPages;

import common.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.BasePage;

import java.util.logging.Logger;

public class FileUploadPage extends BasePage {

    private static final PageElement fileUploadLocator = new PageElement("Upload Files", By.id("file-upload"), true);
    private static final PageElement submitButtonLocator = new PageElement("Submit Button", By.id("file-submit"), true);
    private static final PageElement uploadedFilesLocator = new PageElement("Uploaded files", By.id("uploaded-files"), true);

    public FileUploadPage(WebDriver driver, Logger log) {
        super(driver, log);
    }

    //Push upload button
    public void clickUploadButton(){
        click(submitButtonLocator);
    }
    public void selectFile(String fileName){
        log.info("Selecting '" + fileName + "' file from Files folder");
        String filePath = System.getProperty("user.dir") + "\\src\\main\\resources\\" + fileName;
        type(filePath, fileUploadLocator);
        log.info("File selected");
    }
    public String getUploadedFileNames(){
        String names = find(uploadedFilesLocator).getText();
        log.info("Uploaded files: " + names);
        return names;
    }
}
