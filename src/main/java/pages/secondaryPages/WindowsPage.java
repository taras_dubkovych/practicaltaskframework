package pages.secondaryPages;
import common.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.BasePage;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class WindowsPage extends BasePage {


    private static final PageElement clickHereLink = new PageElement("Click Here Link", By.linkText("Click Here"), true);

    public WindowsPage(WebDriver driver, Logger log) {
        super(driver, log);
    }

    public boolean pageIsDisplayed() {
        return false;
    }

    public void openNewWindow(){

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        click(clickHereLink);

    }
    public void switchWindow(WebDriver driver){
        for(String windowHandle:driver.getWindowHandles()){
            driver.switchTo().window(windowHandle);
        }
    }

}
