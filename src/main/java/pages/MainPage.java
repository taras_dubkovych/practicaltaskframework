package pages;
import common.PageElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import java.util.logging.Logger;

public class MainPage extends BasePage {
    private static final PageElement multipleWindowsLink = new PageElement("Multiple Windows Link", By.linkText("Multiple Windows"), true);
    private static final PageElement WYSIWYGEditorLink = new PageElement("Click WYSIWYG Editor Link", By.linkText("WYSIWYG Editor"), true);
    private static final PageElement fileUpload = new PageElement("Click File Upload Link", By.linkText("File Upload"), true);

    public MainPage(WebDriver driver,  Logger log) {
        super(driver, log);
    }

    public void clickMultipleWindowsLink(){
        click(multipleWindowsLink);
    }
    public void clickWyswygEditorLink(){
        click(WYSIWYGEditorLink);
    }
    public void clickFileUpload (){
        click(fileUpload);
    }
}
