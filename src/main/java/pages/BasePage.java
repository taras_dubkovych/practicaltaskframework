package pages;
import common.PageElement;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.*;
import java.security.SecureRandom;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import org.openqa.selenium.JavascriptExecutor;

public abstract class BasePage {
    protected WebDriver driver;
//    public JavascriptExecutor executor;
    protected Logger log;

    public BasePage(WebDriver driver, Logger log){
        this.driver = driver;
        this.log = log;
//        this.executor = (JavascriptExecutor)driver;
    }


    public String getText(PageElement pageElement){
        log.info("Getting text of element: " + pageElement.getName());
        return this.find(pageElement).getText();
    }

    public void enterText(PageElement pageElement, String text){
        this.enterText(pageElement, text, true);
    }

    public void enterText(PageElement pageElement, String text, boolean clearField){
        log.info("Entering text \"" + text + "\" to element: " + pageElement.getName());
        this.find(pageElement).click();
        if (clearField) {
            this.find(pageElement).clear();
        }
        this.find(pageElement).sendKeys(text);
    }
//    public void javascriptExcutor(PageElement pageElement){
//        log.info("Clicking on element: " + pageElement.getName());
//        this.executor.executeScript("arguments[0].click();", find(pageElement.getLocator()));
//    }

    public void click(PageElement pageElement){
        log.info("Clicking on element: " + pageElement.getName());
        this.find(pageElement).click();
    }
    public void click(By locator){
        log.info("Clicking on element: ");
        find(locator).click();
    }

    public WebElement find(By element){
        return this.driver.findElement(element);
    }

    public WebElement find(PageElement element){
        return this.find(element.getLocator());
    }
    /** Get source of current page */
    public String getCurrentPageSource() {
        return driver.getPageSource();
    }

    /** Get title of current page */
    public String getCurrentPageTitle() {
        return driver.getTitle();
    }

    public void selectFromDropdown(PageElement dropdown){
        Select listDropdown = new Select(find(dropdown.getLocator()));
        List<WebElement> list = listDropdown.getOptions();
        listDropdown.selectByIndex(generateRandomInteger(0, list.size()-1));
    }

    //Switch to iFrame using it's locator
    protected void switshToFrame (PageElement element){
        driver.switchTo().frame(find(element.getLocator()));
    }
    /**
     * Wait for specific ExpectedCondition for the given amount of time in seconds
     */
    private void waitFor(ExpectedCondition<WebElement> condition, Integer timeOutInSeconds) {
        timeOutInSeconds = timeOutInSeconds != null ? timeOutInSeconds : 30;
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(condition);
    }
    /**
     * Wait for given number of seconds for element with given locator to be visible
     * on the page
     */
    protected void waitForVisibilityOf(By locator, Integer... timeOutInSeconds) {
        int attempts = 0;
        while (attempts < 2) {
            try {
                waitFor(ExpectedConditions.visibilityOfElementLocated(locator),
                        (timeOutInSeconds.length > 0 ? timeOutInSeconds[0] : null));
                break;
            } catch (StaleElementReferenceException e) {
            }
            attempts++;
        }
    }
    /** Type given text into element with given locator */
    protected void type(String text, PageElement element) {
        waitForVisibilityOf(element.getLocator(), 5);
        find(element.getLocator()).sendKeys(text);
    }
    public static int generateRandomInteger(int min, int max) {
        SecureRandom rand = new SecureRandom();
        rand.setSeed(new Date().getTime());
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }
    public void waitToBeClickable(By element, int timeout){
        WebDriverWait wait = new WebDriverWait(this.driver, timeout);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }
    public void waitToBeClickable(PageElement element, int timeout){
        WebDriverWait wait = new WebDriverWait(this.driver, timeout);
        wait.until(ExpectedConditions.elementToBeClickable(element.getLocator()));
    }

    public void switchToWindowWithTitle(String expectedTitle){
        String firstWindow = driver.getWindowHandle();

        Set<String> allWindows = driver.getWindowHandles();
        Iterator<String> windowsIterator = allWindows.iterator();

        while (windowsIterator.hasNext()){
            String windowHandle = windowsIterator.next().toString();
            if (!windowHandle.equals(firstWindow)){
                driver.switchTo().window(windowHandle);
                if (getCurrentPageTitle().equals(expectedTitle));
                break;
            }
        }
    }

}
