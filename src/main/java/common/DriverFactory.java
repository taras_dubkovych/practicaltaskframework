package common;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class DriverFactory {
    private String browser;
    private WebDriver driver;
    private Logger log;

    public DriverFactory(String browser, Logger log){
        this.browser = browser.toLowerCase();
        this.log = log;
    }

    public WebDriver getDriver(){
        setUpDriver(browser);
        return driver;
    }

    private void setUpDriver( String browser){

        if ("chrome".equals(browser)) {
            initChromeDriverPath();
            driver = new ChromeDriver();

        } else if ("firefox".equals(browser)) {
            initFirefoxDriverPath();
            driver = new FirefoxDriver();

        }

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);


    }

    private void initFirefoxDriverPath(){
        String chromeDriverPath = System.getProperty("user.dir")+  "\\src\\main\\java\\drivers\\geckodriver.exe";
        System.setProperty("webdriver.gecko.driver", chromeDriverPath);
    }

    private void initChromeDriverPath(){
        String chromeDriverPath = System.getProperty("user.dir")+  "\\src\\main\\java\\drivers\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", chromeDriverPath);
    }


}
