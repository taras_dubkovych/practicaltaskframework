package common;
import org.openqa.selenium.By;

public class PageElement {
    private final String name;
    private final By locator;
    private final boolean required;

    public PageElement(String name, By locator, boolean required) {
        this.name = name;
        this.locator = locator;
        this.required = required;
    }
    public String getName(){
        return name;
    }
    public By getLocator(){
        return locator;
    }
    public boolean getRequired(){
        return required;
    }

    public PageElement(String name, By locator){
        this(name, locator, false);
    }

}
