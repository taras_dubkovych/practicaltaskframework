package tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ExampleTest extends BaseTest {

    @Test(description = "Log in with valid credentials")
    public void newWindowTest() throws InterruptedException {
        log.info("Test starded");
        driver.get("http://the-internet.herokuapp.com/");
        mainPage.clickMultipleWindowsLink();
        windowsPage.openNewWindow();
        windowsPage.switchToWindowWithTitle("New Window");
        String pageSource = windowsPage.getCurrentPageSource();
        // Verification that new page contains expected text in source
        Assert.assertTrue(pageSource.contains("New Window"), "New page source doesn't contain expected text");
    }
}
