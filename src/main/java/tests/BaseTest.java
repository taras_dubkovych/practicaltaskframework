package tests;

import common.DriverFactory;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.*;
import pages.*;
import pages.secondaryPages.FileUploadPage;
import pages.secondaryPages.IframePage;
import pages.secondaryPages.WindowsPage;

import java.lang.reflect.Method;
import java.util.logging.LogManager;
import java.util.logging.Logger;


public class BaseTest {

    protected MainPage mainPage;
    protected WindowsPage windowsPage;
    protected IframePage iframePage;
    protected FileUploadPage fileUploadPage;

    protected String testSuiteName;
    protected String testName;
    protected String testMethodName;

    protected WebDriver driver;
    protected Logger log = Logger.getLogger(getClass().getCanonicalName());

    @BeforeMethod
    @Parameters("browser")
    public void setupTestRun(Method method, @Optional("chrome") String browserName,  ITestContext ctx){
        String testName = ctx.getCurrentXmlTest().getName();
        driver = new DriverFactory(browserName, log).getDriver();
        initPages();
        this.testSuiteName = ctx.getSuite().getName();
        this.testName = testName;
        this.testMethodName = method.getName();
    }

    @AfterMethod
    public void turnDown(){
        if(driver != null) {
            driver.quit();
            driver = null;
        }
    }

    private void initPages(){
        mainPage = new MainPage(driver, log);
        windowsPage = new WindowsPage(driver, log);
        iframePage = new IframePage(driver, log);
        fileUploadPage = new FileUploadPage(driver, log);
    }
//    protected WebDriver getWebDriver(){
//        return driver;}
//
//    @AfterMethod
//    public void closeWebdriver() {
//        getWebDriver().quit();
//    }
}
