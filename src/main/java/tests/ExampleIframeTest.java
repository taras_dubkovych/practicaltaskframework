package tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ExampleIframeTest extends BaseTest {
    @Test(description = "Checking of default editor text")
    public void iframeTest() throws InterruptedException {
        log.info("Test starded");
        driver.get("http://the-internet.herokuapp.com/");
        mainPage.clickWyswygEditorLink();
        String editorText = iframePage.getEditorText();
        // Verification that new page contains expected text in source
        Assert.assertTrue(editorText.equals("Your content goes here."), "Editor default text is net expected. It is: " + editorText);
    }
}
