package tests;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ExampleFileUploadingTest extends TestUtilities {

    //@Parameters({"number", "fileName"})
    @Test(dataProvider = "files")
    public void uploadFilesTest(int number, String fileName) throws InterruptedException {
        log.info("Test started №" + number + " for " + fileName);
        driver.get("http://the-internet.herokuapp.com/");
        mainPage.clickFileUpload();

        //select file
//        String fileName = "1.txt";
        fileUploadPage.selectFile(fileName);

        //click upload button
        fileUploadPage.clickUploadButton();

        //Get upload files
        String fileNames = fileUploadPage.getUploadedFileNames();

        // Verification that new page contains expected text in source
        Assert.assertTrue(fileNames.contains(fileName), "Our file (" + fileName +") is not one of the uploaded (" + fileNames +")");
        takeScreenshot("Files uploaded");
    }
}
